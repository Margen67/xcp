#include "svod.h"

#include "util.h"
#include "xcp.h"

int DecompressSVOD(XCP_CONTEXT *xcp_context, z_stream *strm) {
  uint8_t data_block = 0;
  uint64_t data_buff_size = 0;
  uint64_t last_pos = strm->total_in;
  uint64_t remainder = xcp_context->decompressed_size;
  vector<uint8_t>::const_iterator data_buff_begin;

  int ret = 0;

  printf("XCP Contains SVOD. Splitting into SVOD Format...\n");

  stringstream path;
  wstring title_wstring = xcp_context->title;
  string title_string = narrow(title_wstring);

  printf("Creating Game Directory...\n");
  path << title_string << "/" << uppercase << setfill('0') << setw(4) << hex
       << xcp_context->title_id;

  MKDIR(title_string.c_str());
  MKDIR(path.str().c_str());
  path << "/00007000";
  MKDIR(path.str().c_str());

  printf("Creating Data Directory...\n");
  MKDIR((path.str() + string("/") + xcp_context->directory_name).c_str());
  xcp_context->directory_name =
      path.str() + string("/") + xcp_context->directory_name;

  printf("Writing SVOD Header...\n");
  FILE *header = fopen(
      (path.str() + string("/") + string(xcp_context->out)).c_str(), "wb+");
  fwrite(xcp_context->data_buff.data(), HEADER_SIZE, 1, header);
  xcp_context->data_buff.erase(xcp_context->data_buff.begin(),
                               xcp_context->data_buff.begin() + HEADER_SIZE);
  fclose(header);

  printf("Writing Data%04d...\n", data_block);
  char *temp = new char[9];
  snprintf(temp, 9, "Data%04d", data_block);
  string data_name = xcp_context->directory_name + "/" + string(temp);
  FILE *data = fopen(data_name.c_str(), "wb+");

  size_t written_data = 0;

  while (last_pos < xcp_context->size) {
    FSEEK64(xcp_context->xcp, last_pos, SEEK_SET);
    ret = inf(strm, xcp_context->xcp, &xcp_context->data_buff);
    if (ret != Z_OK)
      zerr(ret);

    data_buff_size = xcp_context->data_buff.size();
    data_buff_begin = xcp_context->data_buff.begin();

    if (written_data + data_buff_size >= DATA_SIZE) {
      fwrite(xcp_context->data_buff.data(), DATA_SIZE - written_data, 1, data);
      fclose(data);
      data_block++;

      xcp_context->data_buff.erase(
          data_buff_begin, data_buff_begin + (DATA_SIZE - written_data));

      printf("Writing Data%04d...\n", data_block);
      temp = new char[9];
      snprintf(temp, 9, "Data%04d", data_block);
      data_name = xcp_context->directory_name + "/" + string(temp);
      data = fopen(data_name.c_str(), "wb+");

      fwrite(xcp_context->data_buff.data(), xcp_context->data_buff.size(), 1,
             data);

      written_data = xcp_context->data_buff.size();
      xcp_context->data_buff.erase(data_buff_begin,
                                   xcp_context->data_buff.end());
    } else {
      fwrite(xcp_context->data_buff.data(), data_buff_size, 1, data);
      xcp_context->data_buff.erase(data_buff_begin,
                                   xcp_context->data_buff.end());
      written_data += data_buff_size;
    }

    last_pos += strm->total_in;
  }
  fclose(xcp_context->xcp);

  return ret;
}
