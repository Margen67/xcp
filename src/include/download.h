#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include "util.h"
#include "xcp.h"

using namespace std;

int download_xcp(XCP_CONTEXT *xcp_context, const char *guid, bool download);

#endif
