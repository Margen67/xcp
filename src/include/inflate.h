#ifndef INFLATE_H
#define INFLATE_H

#include "util.h"
#include <iostream>
#include <vector>
#include <zlib.h>

#define CHUNK (256 * 1024)         /*256KB Decompress*/
#define HEADER_SIZE (44 * 1024)    /*SVOD Header 44KB*/
#define DATA_SIZE 170459136        /*Data Block ~162MB*/
#define DATA_BUFF_SIZE (64 * 1024) /*64KB R/W Buffer*/

int inf(z_stream *strm, FILE *source, vector<uint8_t> *buff);
void zerr(int ret);

#endif
