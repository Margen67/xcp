#ifndef _BASE64_H
#define _BASE64_H

#include <string>

using namespace std;

static const string base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                   "abcdefghijklmnopqrstuvwxyz"
                                   "0123456789+/";

string base64_decode(string const &encoded_string);

#endif
