#ifndef SVOD_H
#define SVOD_H

#include "util.h"
#include "xcp.h"

using namespace std;

int DecompressSVOD(XCP_CONTEXT *xcp_context, z_stream *strm);

#endif
