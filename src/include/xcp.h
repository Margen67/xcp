#ifndef XCP_H
#define XCP_H

#include "inflate.h"

#define XCP_MAGIC 0x789C

using namespace std;

typedef struct XCP_CONTEXT {
  char *in;
  char *out;
  string filename;
  string contentIdHex;
  bool isSVOD;
  uint8_t buff[2];
  wchar_t title[0x40];
  uint32_t title_id;
  string title_id_hex;
  uint64_t size = 0;
  uint64_t decompressed_size = 0;
  string directory_name;
  string region;
  FILE *xcp = nullptr;
  vector<uint8_t> data_buff;
} _XCP_CONTEXT;

bool CheckHeader(XCP_CONTEXT *xcp_context);
bool SetupContext(XCP_CONTEXT *xcp_context);
int ReadHeader(XCP_CONTEXT *xcp_context, z_stream *strm);
int PrintHeader(XCP_CONTEXT *xcp_context, z_stream *strm);

#endif
