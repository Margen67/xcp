#ifndef UTIL_H
#define UTIL_H

#include <errno.h>
#include <iomanip>
#include <locale.h>
#include <sstream>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#include <direct.h>
#include <intrin.h>
#define GETCWD _getcwd
#define MKDIR _mkdir
#define BYTESWAP16 _byteswap_ushort
#define BYTESWAP32 _byteswap_ulong
#define BYTESWAP64 _byteswap_uint64
#define FOPEN64 _fopen64
#define FTELL64 _ftelli64
#define FSEEK64 _fseeki64
#else
#include <sys/stat.h>
#include <unistd.h>
#define GETCWD getcwd
#define MKDIR(X) mkdir(X, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#ifdef __ORDER_LITTLE_ENDIAN__
#define BYTESWAP16 __builtin_bswap16
#define BYTESWAP32 __builtin_bswap32
#define BYTESWAP64 __builtin_bswap64
#else
#define BYTESWAP16
#define BYTESWAP32
#define BYTESWAP64
#endif
#if defined(__FreeBSD__) || defined(__APPLE__)
#define FOPEN64 fopen
#define FTELL64 ftell
#define FSEEK64 fseek
#else
#define FOPEN64 fopen64
#define FTELL64 ftello64
#define FSEEK64 fseeko64
#endif
#endif

using namespace std;

void byteswap16_utf_wchar(wchar_t *dest, uint16_t *src, uint64_t size);
string narrow(const wstring &str);
bool isValidGUID(string str);

#endif
