#include "inflate.h"

int inf(z_stream *strm, FILE *source, vector<uint8_t> *buff) {
  int ret;
  unsigned have;
  uint8_t in[CHUNK];
  uint8_t out[CHUNK];

  strm->zalloc = Z_NULL;
  strm->zfree = Z_NULL;
  strm->opaque = Z_NULL;
  strm->avail_in = 0;
  strm->next_in = Z_NULL;
  ret = inflateInit(strm);
  if (ret != Z_OK)
    return ret;

  do {
    strm->avail_in = fread(in, 1, CHUNK, source);
    if (ferror(source)) {
      (void)inflateEnd(strm);
      return Z_ERRNO;
    }
    if (strm->avail_in == 0)
      break;
    strm->next_in = in;

    do {
      strm->avail_out = CHUNK;
      strm->next_out = out;
      ret = inflate(strm, Z_NO_FLUSH);
      switch (ret) {
      case Z_NEED_DICT:
        ret = Z_DATA_ERROR;
      case Z_DATA_ERROR:
      case Z_MEM_ERROR:
        (void)inflateEnd(strm);
        return ret;
      }
      have = CHUNK - strm->avail_out;
      buff->insert(buff->end(), out, out + have);
    } while (strm->avail_out == 0);

  } while (ret != Z_STREAM_END);

  (void)inflateEnd(strm);
  return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

void zerr(int ret) {
  fputs("zpipe: ", stderr);
  switch (ret) {
  case Z_ERRNO:
    if (ferror(stdin))
      fputs("error reading stdin\n", stderr);
    if (ferror(stdout))
      fputs("error writing stdout\n", stderr);
    break;
  case Z_STREAM_ERROR:
    fputs("invalid compression level\n", stderr);
    break;
  case Z_DATA_ERROR:
    fputs("invalid or incomplete deflate data\n", stderr);
    break;
  case Z_MEM_ERROR:
    fputs("out of memory\n", stderr);
    break;
  case Z_VERSION_ERROR:
    fputs("zlib version mismatch!\n", stderr);
  }
}
