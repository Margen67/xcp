#include "xcp.h"
#include "download.h"
#include "util.h"

bool CheckHeader(XCP_CONTEXT *xcp_context) {
  if (xcp_context->xcp == NULL) {
    printf("%s\n", strerror(errno));
    return false;
  }
  if (fread(xcp_context->buff, 2, 1, xcp_context->xcp) == 0) {
    fprintf(stderr, "Failed reading file\n");
  }

  if (BYTESWAP16(*(uint16_t *)&xcp_context->buff) != XCP_MAGIC) {
    printf("\nPotentially Encrypted XCP, Skipping...\n");
    return false;
  }

  return true;
}

bool SetupContext(XCP_CONTEXT *xcp_context) {
  xcp_context->xcp = FOPEN64(xcp_context->in, "rb");
  if (!CheckHeader(xcp_context)) {
    printf("Terminating...\n");
    return false;
  }

  FSEEK64(xcp_context->xcp, 0, SEEK_END);
  xcp_context->size = FTELL64(xcp_context->xcp);
  FSEEK64(xcp_context->xcp, 0, SEEK_SET);

  return true;
}

int ReadHeader(XCP_CONTEXT *xcp_context, z_stream *strm) {
  int ret = inf(strm, xcp_context->xcp, &xcp_context->data_buff);
  if (ret != Z_OK)
    zerr(ret);

  xcp_context->isSVOD = BYTESWAP32(*(uint32_t *)&xcp_context->data_buff[0x3A9]);
  xcp_context->decompressed_size =
      BYTESWAP64(*(uint64_t *)&xcp_context->data_buff[0x3A1]);
  xcp_context->directory_name = string(xcp_context->out) + string(".data");

  uint16_t title_buff[0x40];

  memcpy(title_buff, (&*xcp_context->data_buff.begin() + 0x1691), 0x80);
  byteswap16_utf_wchar(xcp_context->title, title_buff, 0x40);

  xcp_context->title_id =
      BYTESWAP32(*(uint32_t *)&xcp_context->data_buff[0x360]);

  return ret;
}

int PrintHeader(XCP_CONTEXT *xcp_context, z_stream *strm) {
  printf("\nDecompressing Header...\n\n");

  if (ReadHeader(xcp_context, strm) != Z_OK) {
    printf("Could not decompress header\n");
    return -1;
  }

  printf("%-24.24s %ls\n", "Title:", xcp_context->title);
  printf("%-24.24s %08X\n", "TitleID:", xcp_context->title_id);
  printf("%-24.24s %08X\n",
         "MediaID:", BYTESWAP32(*(uint32_t *)&xcp_context->data_buff[0x354]));
  printf("%-24.24s %lu MB\n",
         "Compressed Size:", xcp_context->size / (1024 * 1024));
  printf("%-24.24s %lu MB\n\n", "Decompressed Size:",
         (xcp_context->decompressed_size +
          (xcp_context->isSVOD ? HEADER_SIZE : 0)) /
             (1024 * 1024));

  return 0;
}
