#include "download.h"
#include "HTTPRequest.hpp"
#include "base64.h"
#include "tinyxml2.h"
#include "util.h"
#include "xcp.h"
#include <chrono>
#include <curl/curl.h>
#include <exception>

typedef chrono::high_resolution_clock Clock;

char element_list[][32] = {
    "a:entry",         "offerInstances", "offerInstance",
    "productInstance", "contentId",
};

Clock::time_point timer, start_time, end_time;
size_t data_rate = 0;
size_t temp = 0;

size_t download_write(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  if (chrono::duration_cast<chrono::nanoseconds>(Clock::now() - timer).count() /
          1000000000 >=
      1) {
    timer = Clock::now();
    data_rate = temp;
    temp = 0;
  }

  temp += size * nmemb;

  return fwrite(ptr, size, nmemb, stream);
}

static int download_progress(void *ptr, double TotalToDownload,
                             double Downloaded, double TotalToUpload,
                             double Uploaded) {
  if (Downloaded != -1.0) {
    printf("\r%.0lfMB / %.0lfMB %.1lf%% %.1fMB/s %lus",
           (Downloaded / 1024.0 / 1024.0), (TotalToDownload / 1024.0 / 1024.0),
           100.0 * (Downloaded / TotalToDownload),
           ((float)(data_rate / 1024) / 1024.0f),
           chrono::duration_cast<chrono::nanoseconds>(Clock::now() - start_time)
                   .count() /
               1000000000);
  }

  fflush(stdout);
  return 0;
}

int handle_xml(XCP_CONTEXT *xcp_context, const char *guid) {
  string url =
      "http://marketplace-xb.xboxlive.com/marketplacecatalog/v1/product/" +
      xcp_context->region + "/";
  string query =
      "?bodytypes=1.3&detailview=detaillevel5&pagenum=1&stores=1&tiers=1.3&"
      "offerfilter=1&producttypes=1.5.18.19.20.21.22.23.30.34.37.46.47.61";
  url += string(guid) + query;

  http::Request request(url.c_str());
  http::Response responce = request.send("GET");

  tinyxml2::XMLDocument doc;
  tinyxml2::XMLElement *element;

  doc.Parse((const char *)responce.body.data(), responce.body.size());

  const char *contentId;
  const char *titleId;

  if (doc.FirstChildElement("a:feed") != nullptr)
    element = doc.FirstChildElement("a:feed")->ToElement();
  else {
    printf("Could not get 'a:feed' element!\n");
  }

  for (int i = 0; i < sizeof(element_list) / 32; i++) {
    if (element->FirstChildElement(element_list[i]) != nullptr)
      element = element->FirstChildElement(element_list[i])->ToElement();
    else {
      printf("Could not get %s element!\n", element_list[i]);
      return -1;
    }
  }
  contentId = element->GetText();

  try {
    titleId = doc.FirstChildElement("a:feed")
                  ->FirstChildElement("a:entry")
                  ->FirstChildElement("hexTitleId")
                  ->GetText();
  } catch (exception e) {
    fprintf(stderr, "Exception: %s\n", e.what());
    return -1;
  }

  string titleIdHex(titleId);

  titleIdHex.erase(0, 2);

  string temp(contentId);
  temp = base64_decode(temp);
  stringstream temp2;

  for (int i = 0; i < 20; i++) {
    temp2 << hex << setw(2) << setfill('0')
          << (static_cast<int>(temp[i]) & 0xFF);
  }

  xcp_context->title_id_hex = titleIdHex;
  xcp_context->contentIdHex = temp2.str();
  xcp_context->filename = xcp_context->contentIdHex + string(".xcp");

  return 0;
}

int download_xcp(XCP_CONTEXT *xcp_context, const char *guid, bool download) {
  printf("Checking %s...\n", guid);

  if (handle_xml(xcp_context, guid) != 0) {
    printf("Could not fetch product data!\n");
    return -1;
  }

  CURL *curl;
  FILE *download_file;
  CURLcode res;

  curl = curl_easy_init();
  if (curl) {
    string url = string("http://download.xboxlive.com/content/") +
                 xcp_context->title_id_hex + string("/") +
                 xcp_context->filename;
    xcp_context->in = &xcp_context->filename[0u];
    xcp_context->out = &xcp_context->contentIdHex[0u];

    if (!download) {
      printf("URL: %s\n", url.c_str());
      return 0;
    }

    download_file = FOPEN64(xcp_context->filename.c_str(), "wb+");
    printf("Downloading: %s\n", url.c_str());
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, download_write);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, download_file);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, false);
    curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, download_progress);

    start_time = Clock::now();
    res = curl_easy_perform(curl);
    end_time = Clock::now();

    printf("\nFinished in %lu seconds!\n",
           chrono::duration_cast<chrono::nanoseconds>(end_time - start_time)
                   .count() /
               1000000000);

    curl_easy_cleanup(curl);
    fclose(download_file);
  }

  return 0;
}
