#include "util.h"
#include <regex>
#include <sstream>

void byteswap16_utf_wchar(wchar_t *dest, uint16_t *src, uint64_t size) {
  for (int i = 0; i < size; i++) {
    dest[i] = BYTESWAP16(src[i]);
  }
}

string narrow(const wstring &str) {
  ostringstream stm;
  const ctype<char> &ctfacet = use_facet<ctype<char>>(stm.getloc());
  for (size_t i = 0; i < str.size(); ++i)
    stm << ctfacet.narrow(str[i], 0);
  return stm.str();
}

bool isValidGUID(string str) {
  const regex pattern(
      "^[{]?[0-9a-fA-F]{8}-([0-9a-fA-F]{4}-){3}[0-9a-fA-F]{12}[}]?$");

  if (str.empty()) {
    return false;
  }

  if (regex_match(str, pattern)) {
    return true;
  } else {
    return false;
  }
}
