#include "download.h"
#include "svod.h"
#include "util.h"
#include "xcp.h"

z_stream strm;

bool extract = false;
bool download = false;
string region = "en-US";

int handle_args(vector<string> *guids, int argc, char *argv[]) {
  if (argc > 0) {
    for (int i = 1; i < argc; i++) {
      if (strstr(argv[i], "\n")) {
        strncpy(argv[i], argv[i], strlen(argv[i]) - 1);
      }

      if (strcmp(argv[i], "-d") == 0) {
        download = true;
      } else if (strcmp(argv[i], "-g") == 0) {
        download = false;
      } else if (strcmp(argv[i], "-e") == 0) {
        extract = true;
      } else if (strcmp(argv[i], "-r") == 0) {
        region = string(argv[i + 1]);
        i++;
      } else if (isValidGUID(string(argv[i]))) {
        guids->push_back(string(argv[i]));
      } else {
        printf("Unknown Arg: %s\n", argv[i]);
      }
    }
  } else {
    printf("xcp -d <GUID>\nxcp -g <GUID>xcp -d -e <GUID>\n");
    return -1;
  }
  return 0;
}

int main(int argc, char *argv[]) {
  int ret = 0;

  vector<string> guids;
  setlocale(LC_ALL, "");

  if (handle_args(&guids, argc, argv) != 0) {
    printf("Failed to Parse Arguments\n");
    return 0;
  } else {
    for (size_t i = 0; i < guids.size(); i++) {
      XCP_CONTEXT xcp_context;
      xcp_context.region = region;
      ret = download_xcp(&xcp_context, guids[i].c_str(), download);
      if (ret != 0) {
        printf("Failed to fetch XCP %s\n", guids[i].c_str());
        continue;
      }

      if (download) {
        if (!SetupContext(&xcp_context)) {
          printf("Could not setup XCP context!\n");
          continue;
        }

        if (PrintHeader(&xcp_context, &strm) != 0) {
          continue;
        }

        // Get the first Zlib segment
        if (extract) {
          if (xcp_context.isSVOD) {
            if (DecompressSVOD(&xcp_context, &strm) != Z_OK) {
              printf("Could not decompress SVOD\n");
              continue;
            }
          } else {
            printf("XCP is not Game on Demand. Skipping...\n");
            continue;
          }
        } else {
          printf("Skipping Extraction...\n");
        }
      }
    }
  }

  return 0;
}
