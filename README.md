# XCP

A simple XCP decompressor written in C++.

### Prerequisites

You need the following libraries installed:
  * tinyxml2
  * curl
  * zlib

### Building

The Makefile uses g++ by default. You can use either g++ or clang to compile.

Compile:
  ```
  make
  ```

### Usage

Download an XCP:
  ```
  ./xcp -d <GUID>
  ```

Download and decompress an XCP:
  ```
  ./xcp -d -e <GUID>
  ```

Generate a URL to an XCP:
  ```
  ./xcp -g <GUID>
  ```
